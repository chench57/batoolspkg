#######################################################################################
#######################################################################################

#BayesA MCMC approach
GBLUPe = function(dataobj=NULL,op=NULL,y=NULL,Z=NULL,G=NULL,X=NULL)  
#  startpi is defined by the proportion of markers that ARE associate with genes
{   # START OF FUNCTION

  set.seed(op$seed)  
  
# Parameters
  if(!is.null(dataobj))
  {
	Z=dataobj$geno
	if(is.null(trait)){
		y=dataobj$pheno[,1,]	
	}else{
		y=dataobj$pheno[,trait,]
	}	
  }
  whichNa=which(is.na(y))
  if(length(whichNa)>0)
  {
	Z0=Z
	y0=y
	y=y[-whichNa]
	Z=Z[-whichNa,]
  }
  nSNP=dim(Z)[2]  #number of SNP
  nanim=dim(Z)[1]; #number of animals

  	if(op$poly)
	{
		Ainv<-dataobj$Ainv
		Zu<-diag(rep(1,nanim))
	  	pU<-dim(Zu)[1]
		Zu2<-apply(Zu,2,crossprod) #colSums(Zu*Zu)
		ueff<-array(0,pU)
		names(ueff)<-rownames(Ainv)
		varusave=array(0,numiter/skip)
		varu<-op$init$varu
		# This is for computing mean and variance of each SNP polygenenic effect.
		Mulast =    array(0,pU)
		Qulast =    array(0,pU)
		Mucurrent = array(0,pU)
		Qucurrent = array(0,pU)
	}

  
  
  

 

  # input data
  nrecords = length(y)
if(!is.null(X))
  {
      	X = X
	      dimX = dim(X)[2]
	if(length(whichNa)>0)
  	{
		X0=X
		X=X[-whichNa,]
  	}
  }else{
  	X = as.matrix(rep(1,nrecords))
	dimX = 1
  }


	scalea  = op$init$scale
##########start BLUP############  


maxiter=3000

seed=op$seed
#system("rm log.txt")
set.seed(seed)


W = cbind(X,Z)

WWdiag = apply(W,2,crossprod) # diagonals are important

dimW = dim(W)[2]  # number of columns = # of location parameters

# initialize a few arrays

theta       = array(0,dimW)
# so this is the residual.
ycorr = y - as.vector(W%*%theta)   # adjust obs by everything before starting iterations


convcrit = 1E-4
convcurr = 1E10
iter = 0

lambda = 100

Za = diag(1,nrecords,nrecords)
XX = crossprod(X)
XZa=  crossprod(X,Za)
ZaX=  crossprod(Za,X)
ZZa=  crossprod(Za)
if(is.null(G)) ZZ_marker =  crossprod(t(Z))
else ZZ_marker=G
W = cbind(X,Za)
thetakeep = array(0,ncol(W))
Wy=crossprod(W,y)
tscale=rep(0,2)
tvare=rep(0,2)
tcon=rep(0,2)
tlog=rep(0,2)
#tlog1=rep(0,2)
rankX=as.numeric(rankMatrix(X))
derivAI=matrix(0,2,1)
derivcheck=matrix(0,2,1)
inform=matrix(0,2,2)
informcheck =matrix(0,2,2)
while (abs(convcurr) > convcrit)
{
  
  iter = iter+1
  Ainv=solve(ZZ_marker)
  ZZ_G=ZZa+Ainv*lambda
  coeff=rbind( cbind(XX,XZa),
               cbind(ZaX,ZZ_G))
  C=solve(coeff)
  theta=C%*%Wy
  u=theta[-c(1:dimX)]
  ycorr=y-W%*%theta
  
  if (iter==1) {
    vare=as.numeric(crossprod(y,ycorr)/(nrecords-rankX))
    scalea=vare/lambda   
  }
  
  Cgg=C[(dimX+1):(dimX+nanim),(dimX+1):(dimX+nanim)]*as.numeric(vare)
  fsigma2e=ycorr/as.numeric(vare)
  Pfsigma2e=(fsigma2e-W%*%C%*%t(W)%*%fsigma2e)/as.numeric(vare)
  
  fsigma2u=u/as.numeric(scalea)
  Pfsigma2u=(fsigma2u-W%*%C%*%t(W)%*%fsigma2u)/as.numeric(vare)
  
  informAI=rbind( cbind(t(fsigma2e)%*%Pfsigma2e,t(fsigma2e)%*%Pfsigma2u),
                  cbind(t(fsigma2u)%*%Pfsigma2e,t(fsigma2u)%*%Pfsigma2u))/2    #AI matrix, the document doesn't multiply by 0.5
  traceCgg_a=sum(diag(Ainv%*%Cgg))	
  derivAI[1]=-0.5*((nrecords-rankX)/vare-(nanim-traceCgg_a/as.numeric(scalea))/as.numeric(vare)-crossprod(ycorr)/(vare^2)) #check here
  derivAI[2]=-0.5*(nanim/scalea-traceCgg_a/(scalea^2)-crossprod(u,Ainv%*%u)/as.numeric(scalea^2))  #DL matrix
  
  
  
  vardiff=solve(informAI)%*%derivAI
  
  
  if((vardiff[1]+vare)<=0)
  {
    vare=as.numeric(vare/2)
  }else{
    vare=vare+as.numeric(vardiff[1])
  }
  if((scalea+vardiff[2])<=0)
  {
    
    scalea=as.numeric(scalea/2)
  }else{
   
    scalea=scalea+as.numeric(vardiff[2])
  }
  
  lambda = as.numeric(vare/scalea);
  gamma = 1/lambda;
  
  cat ("Iteration",iter," Residual Variance is ",vare," and ",sep=" ")
  cat ("Genetic Variance is ",scalea,"\n",sep=" ")
  
  V=ZZ_marker*as.numeric(scalea)+diag(nanim)*as.numeric(vare)
  Vinv=solve(V)
  yfixed=y-X%*%theta[1:dimX]
  
  # COMPUTE THIS LOG LIKELIHOOD A LOT MORE EFFICIENTLY!
  logL2=-0.5*(as.numeric(determinant(V)$modulus)+log(det(t(X)%*%Vinv%*%X))+t(yfixed)%*%Vinv%*%yfixed) #This is the log likelihood provide in Meyer, 1989 equation [3]
  #logL1=-0.5*nanim*log(vare)-0.5*nanim*log(as.numeric(scalea))-0.5*as.numeric(determinant(coeff)$modulus)/vare-0.5*t(y)%*%ycorr/vare #the log likeihood provided in the document is not working here
  #XVXinv=solve(t(X)%*%Vinv%*%X)
  #P=Vinv-Vinv%*%X%*%XVXinv%*%t(X)%*%Vinv
  
  
  tscale[iter]=scalea
  tvare[iter]=vare
  thetakeep = cbind(thetakeep,theta)            #keep iterate
  tlog[iter]=logL2
  #tlog1[iter]=logL1
  
  thetakeep = cbind(thetakeep,theta)            #keep iterate
  if(iter>1) convcurr=logL2-tlog[iter-1]
  tcon[iter]=convcurr

}

SNPeffBLUP_a=crossprod(Z,Ainv)%*%u

##########end BLUP###########


  if(length(whichNa)>0)
  {
	Z=Z0
	if(!is.null(dataobj)) X=X0
  }
 	if(op$poly)
	{
		BAout<-list(betahat=theta[1:dimX],ghat=SNPeffBLUP_a, yhat=theta[1]+Z%*%SNPeffBLUP_a+Zu%*%SNPeffBLUP_u,uhat=SNPeffBLUP_u,hypers=c(vare,scalea),Ginv=Ainv,Cuu=Cgg)
	}else BAout<-list(betahat=theta[1:dimX],ghat=SNPeffBLUP_a, yhat=theta[1]+Z%*%SNPeffBLUP_a,hypers=c(vare,scalea),Ginv=Ainv,Cuu=Cgg)
  
  class(BAout)="BAout"
  return(BAout)	
}  # END OF FUNCTION

#######################################################################################
#######################################################################################


# summary function for bayesian model
basummary <- function(dataobj=NULL,BAout=NULL,y=NULL,Z=NULL,yNa=NULL,g=NULL){
	if(is.null(BAout)) stop("BAout must be provided for the function")
	if(!is.null(dataobj))
	{
		acc=cor(dataobj$pheno[,1,],BAout$yhat)
		cat(paste("The prediction accuarcy is ",round(acc,3)))
		cat("\n")
		cat("The effective sample size for hyperparameters are: \n")
		print(BAout$eff_sample)
		#cat("\n")
		cat("The mean of the hyperparameters are: \n")
		print(apply(BAout$hypers,2,mean))
	}else if((!is.null(y)) && (!is.null(Z))){
		if(!is.null(yNa))
		{
			whichNa=which(is.na(yNa))
			cat(paste(length(whichNa),"of individuals are in the validation dataset and ",length(y)-length(whichNa)," are in the training dataset"))
			cat("\n")
			acc=cor(y[whichNa],BAout$yhat[whichNa])
			cat(paste("The cross validation accuarcy is ",round(acc,3)))
			
		}else{
			if(!is.null(g))
			{
				#if(BAout$model=="IWBayesA")
				#{
				#	acc=cor(BAout$x%*%g,BAout$x%*%BAout$ghat)
				#}else 
				acc=cor(Z%*%g,Z%*%BAout$ghat)
				cat(paste("The breeding value prediction accuarcy is ",round(acc,3)))
			}

		}
		cat("\n")
		cat("The effective sample size for hyperparameters are: \n")
		print(BAout$eff_sample)
		cat("\n")
		cat("The mean of the hyperparameters are: \n")
		print(apply(BAout$hypers,2,mean))
	}else{
		stop("baData or y and Z must be provided for the function")
	}

}




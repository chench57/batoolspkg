#######################################################################################
#######################################################################################
rinvgauss<-function (n, mu, lambda = 1) 
{
    if (any(mu <= 0)) 
        stop("mu must be positive")
    if (any(lambda <= 0)) 
        stop("lambda must be positive")
    if (length(n) > 1) 
        n <- length(n)
    if (length(mu) > 1 && length(mu) != n) 
        mu <- rep(mu, length = n)
    if (length(lambda) > 1 && length(lambda) != n) 
        lambda <- rep(lambda, length = n)
    y2 <- rchisq(n, 1)
    u <- runif(n)
    r2 <- mu/(2 * lambda) * (2 * lambda + mu * y2 + sqrt(4 * 
        lambda * mu * y2 + mu^2 * y2^2))
    r1 <- mu^2/r2
    ifelse(u < mu/(mu + r1), r1, r2)
}

#BayesL MCMC approach
BayesLm = function(dataobj=NULL,op=NULL,y=NULL,Z=NULL,yNa=NULL,trait=NULL)  
#  startpi is defined by the proportion of markers that ARE associate with genes
{   # START OF FUNCTION

  set.seed(op$seed)  
  
  # Parameters
  if(!is.null(dataobj))
  {
	Z=dataobj$geno
	if(is.null(trait)){
		y=dataobj$pheno[,1,]	
	}else{
		y=dataobj$pheno[,trait,]
	}
	
  }
  whichNa=which(is.na(y))
  if(length(whichNa)>0)
  {
	Z0=Z
	y0=y
	y=y[-whichNa]
	Z=Z[-whichNa,]
  }
  nSNP=dim(Z)[2]  #number of SNP
  nanim=dim(Z)[1]; #number of animals
	if(op$poly)
	{
		Ainv<-dataobj$Ainv
		Zu<-diag(rep(1,nanim))
	  	pU<-dim(Zu)[1]
		Zu2<-apply(Zu,2,crossprod) #colSums(Zu*Zu)
		ueff<-array(0,pU)
		names(ueff)<-rownames(Ainv)
		varusave=array(0,numiter/skip)
		varu<-op$init$varu
		# This is for computing mean and variance of each SNP polygenenic effect.
		Mulast =    array(0,pU)
		Qulast =    array(0,pU)
		Mucurrent = array(0,pU)
		Qucurrent = array(0,pU)
	}

  # Vectors for saved samples of hyperparameters
  if (op$update_para$df)    {defsave   =  array(0,op$run_para$niter/op$run_para$skip) }
  if (op$update_para$scale)  {scalesave =  array(0,op$run_para$niter/op$run_para$skip) }
  if (op$update_para$pi)     {pisave    =  array(0,op$run_para$niter/op$run_para$skip) } 
  varesave = array(0,op$run_para$niter/op$run_para$skip)

  # To monitor MH acceptance ratios on degrees of freedom parameter
  alphadef_save = array(0,(op$run_para$niter-op$run_para$burnIn))

  # input data
  nrecords = length(y)
  if(!is.null(dataobj))
  {
      	X = dataobj$fixed
	dimX = dim(X)[2]
	if(length(whichNa)>0)
  	{
		X0=X
		X=X[-whichNa,]
  	}
  }else{
  	X = array(1,nrecords)
	dimX = 1
  }

  W = as.matrix(cbind(X,Z))
  Wstacked=as.vector(W);

  # specify prior degrees of belief on genetic variance
  # specify  prior scale on genetic variance
  # inital values   

  def=op$init$df			 # Starting Bayes A df 
  scalea  = op$init$scale    	 # Starting scale parameter
  pip = op$init$pi                   # Starting pi parameter   (probability of being a gene!)                                                          
  cdef       = op$priors$cdef		 # cdef is scale parameter for proposal density on df
  alphacheck = 0
  alphaQTLvar= 0			 # to monitor BayesB QTLvar rejection rates 
  alphatot1= 0
  WWdiag     = apply(W,2,crossprod) # diagonals are important


  # used to monitor Metropolis Hastings acceptance ratio for df
  dimW     = dim(W)[2]  # number of columns = # of location parameters

  # initialize a few arrays
  theta      = array(0,nSNP+1)
  SNPeff     = array(0.001,nSNP)
  varq       = array(scalea,nSNP)  # initialize each SNP variance to overall scale parameter
  SNPvar     = array(0,nSNP)       # used to take running sum of SNP specific variances
  SNPvarlast = array(0,nSNP)
  postprob   = array(0,nSNP)       # indicator variable for inclusion of SNP in model
                                   # later used to determine posterior probability...unique to BayesB
  alphametrovar = array(0,nSNP)    # used to monitor MH acceptance rates for included genes.  
  
  # This is for computing mean and variance of each SNP effect.
  Mlast =    array(0,dimW)
  Qlast =    array(0,dimW)
  Mcurrent = array(0,dimW)
  Qcurrent = array(0,dimW)

  # adjust y
  ycorr = y - as.vector(W%*%theta)   # adjust obs by everything before starting iterations
  # so this is the residual.

  # use this to set up the starting value for the residual variance
  vare = crossprod(ycorr)/(nrecords-dimX)
  
  starttime<-proc.time()
  timeSNP = 0
  timetotal=0

  # mcmc sampling
  for (iter in 1:op$run_para$niter) 
  {  #"START" OF MCMC SAMPLING

     itersave = iter-op$run_para$burnIn
     if (itersave ==1) 
     {
        alphametrovar = array(0,nSNP)  # reinitialize to 0 post burn-in
     }

     #####################################################################################
     # sample intercept
     #####################################################################################
     ycorr    = ycorr + W[,1]*theta[1]
     rhs      = sum(ycorr)/vare        # 1'(y-W*theta)/sigmae
     invLhs   = 1.0/(nrecords/vare)    # 1/(sigmae/n)		
     meancond = rhs*invLhs                          
     theta[1] = rnorm(1,meancond,sqrt(invLhs))       
     ycorr    = ycorr - W[,1]*theta[1]  # take it off again to create new "residual"	

     #####################################################################################	
     # sample variance & effect for each SNP	
     ####################################################################################
     if (pip == 1)  
     { # "START" OF BAYESA
       #####################################################################################
       # sample variance for each SNP (Bayes A)	
       #if(iter==1) mu_v=250 else 
	mu_v=sqrt(2*scalea*as.numeric(vare)/(SNPeff*SNPeff))
	
       tau2=1/rinvgauss(nSNP,mu=mu_v, lambda = 2.5)
       varq = as.numeric(scalea*vare)*tau2 # joint sample
       #####################################################################################
       # sample effect for each SNP (Bayes A)	
       #####################################################################################
       startSNP = proc.time()
       BayesC<- .Call("BayesAC",nSNP,dimX,nanim,Wstacked,WWdiag, theta, ycorr,varq,vare,SNPeff)
       theta=BayesC[[1]]
       ycorr=BayesC[[2]]
       varq=BayesC[[3]]
       vare=BayesC[[4]]
       SNPeff=BayesC[[5]]
     }   # "END" FOR BAYESA

	#sample polygenenic effects
	if(op$poly)
	{
		Avaru=Ainv/as.numeric(varu)
		startPoly=proc.time()
		res1<-.Call("sampleeff",pU,nanim,Zu,Zu2,ueff,ycorr,vare,Avaru)
		ueff=res1[[1]]
		ycorr=res1[[2]]
		endPoly=proc.time()
	        varu<-( t(ueff)%*%Ainv%*%ueff )/rchisq(1,length(ueff)-1) #sample sigmau
	}
     if (itersave>1) 
     {     # "START" OF COMPUTE POSTERIOR MEANS & VARIANCES
           Mcurrent = Mlast + (theta-Mlast)/itersave
           Qcurrent = Qlast + (itersave-1)*((theta-Mlast)^2)/itersave
           Mlast = Mcurrent
           Qlast = Qcurrent 
    
           SNPvar=SNPvarlast + (varq-SNPvarlast)/itersave
           SNPvarlast=SNPvar
		if(op$poly)
		{
			Mucurrent = Mulast + (ueff-Mulast)/itersave
			Qucurrent = Qulast + (itersave-1)*((ueff-Mulast)^2)/itersave
			Mulast =    Mucurrent
			Qulast =    Qucurrent	
		}
     }  # "END" OF COMPUTE POSTERIOR MEANS & VARIANCES
     if (itersave==1) {
		Mlast = theta;SNPvarlast=varq;
		if(op$poly)
		{
			Mulast=ueff;
		}
	} 

     #####################################################################################	
     #   sample residual variance
     #####################################################################################
     vare = ( crossprod(ycorr) +op$priors$tau2_e+sum(SNPeff*SNPeff/(tau2)) )/rchisq(1,nrecords+op$priors$nu_e)   # prior on vare used
     #  scale parameter should be e`e + Se,
     #  degrees of freedom should be n + ve

     #####################################################################################
     # sample degrees of freedom
     #####################################################################################
     if (op$update_para$df)  
     {  #"START" OF SAMPLE THE DEGREES OF FREEDOM
        for (cycle in 1:10){
        def0=def
        logdef0 = log(def0)
        logdef1 = rtnorm(1,logdef0,cdef,upper=log(100))   # proposal log df 
        def1 = exp(logdef1)
        lfullcd0 = nSNP*(0.5*def0*(log(scalea)+logdef0-log(2))-lgamma(0.5*def0))
        lfullcd1 = nSNP*(0.5*def1*(log(scalea)+logdef1-log(2))-lgamma(0.5*def1))
        lfullcd0 = lfullcd0 - (0.5*def0+1)*sum(log(varq[which(varq>0)]))-(0.5*def0*scalea)*sum(1/varq[which(varq>0)]);
        lfullcd1 = lfullcd1 - (0.5*def1+1)*sum(log(varq[which(varq>0)]))-(0.5*def1*scalea)*sum(1/varq[which(varq>0)]);
        lfullcd0 = lfullcd0- 2*log(1+def0)+logdef0   # prior + jacobian                                      
        lfullcd1 = lfullcd1- 2*log(1+def1)+logdef1   # prior + jacobian                          
        alpha1 = exp(lfullcd1-lfullcd0)
        alpha1 = min(alpha1,1)
        if (alpha1 < 1)
        { 
	  u = runif(1)
	  if (u<alpha1) {def = def1}
	  else def = def0
        }
       else {def = def1;}
       alphatot1=alphatot1+alpha1
       if ((cycle%%op$run_para$skip == 0) & (iter <= op$run_para$burnIn))
       {
         alphacheck = alphatot1/10;
         if (alphacheck > .70) { cdef = cdef*1.2}
         else if(alphacheck  < .20) {cdef = cdef*.7}     # tune Metropolis sampler #
         alphatot1 = 0;
       }
       else if ((cycle%%op$run_para$skip == 0) & (iter > op$run_para$burnIn)) 
       {
         alphacheck = alphatot1/op$run_para$skip;
         alphadef_save[itersave/op$run_para$skip] = alphacheck
         alphatot1 = 0
       }
       }
     }   # "END" OF SAMPLE THE DEGREES OF FREEDOM  

     #####################################################################################
     #  sample scale#
     #  based on Gelman Uniform(0,A) prior on scale parameter for "large" A.
     #####################################################################################
     if (op$update_para$scale) 
      {  #"START" OF SAMPLE THE SCALE PARAMETER 
           shape_g =nSNP+op$priors$shape_scale
           scale_g = 0.5*sum(tau2*scalea)+op$priors$rate_scale
           scalea = 2/rgamma(1,shape=shape_g,rate=scale_g)
      } #"END" OF SAMPLE THE SCALE PARAMETER 

       ###########################################################################################
       #store MCMC samples at every "skip"
       ###########################################################################################
       if (iter%%op$run_para$skip == 0) 
        {
            varesave[iter/op$run_para$skip] = vare
            if(op$update_para$df) {defsave[iter/op$run_para$skip] = def}
            if(op$update_para$scale) {scalesave[iter/op$run_para$skip] = scalea}
            if(op$update_para$pi) {pisave[iter/op$run_para$skip] = pip}
	    if(op$poly){varusave[iter/op$run_para$skip]=varu}
        }

       endSNP = proc.time()
       timeSNP = timeSNP + (endSNP[1]-startSNP[1]) 

       endtime = proc.time()
       timetotal = endtime[1]-starttime[1]
       if((op$print_mcmc$piter!=0) & iter%%op$print_mcmc$piter==0)
	{
		tmp=paste("iter= ",iter," vare= ",round(vare,6), "scale= ", round(scalea,8),"timepercycle= ", round(timeSNP/iter,3), "estimated time left=", round(timeSNP/iter*(op$run_para$niter-iter),2),"\n",sep=" ")
		if(op$poly)
		{
			tmp=paste("iter= ",iter," vare= ",round(vare,6), "varg= ", round(varq,6),"varu=",round(varu,3),"timepercycle= ", round(timeSNP/iter,3), "estimated time left=", round(timeSNP/iter*(op$run_para$niter-iter),2),"\n",sep=" ")
		}
		if(op$print_mcmc$print_to=="screen")
		{
			cat(tmp)
		}else{
			write(tmp, file = paste("log",op$seed,".txt",sep=""),append = TRUE)
		}

	}
	
       if (iter%%1000==0) 
       {

       #############################################################################################
       #    PROCESSING MCMC SAMPLES
       #############################################################################################
       hyperparameters = as.matrix(varesave)
       colnames(hyperparameters) = "varesave"
       if (op$update_para$scale) {hyperparameters = cbind(hyperparameters,scalesave) }
       if (op$update_para$df) {hyperparameters = cbind(hyperparameters,defsave) }
       if (op$update_para$pi) {hyperparameters = cbind(hyperparameters,pisave) }
	if(op$poly) {hyperparameters = cbind(hyperparameters,varusave)}
       rownames(hyperparameters) = seq(op$run_para$skip,op$run_para$niter,op$run_para$skip)
       save(iter,hyperparameters,file=paste(op$save.at, "Hyperparameters",op$seed,".RData",sep="")) 
 
       SNPtimepercycle = timeSNP/iter
       timepercycle=timetotal/iter
       save(iter,timepercycle,SNPtimepercycle,file=paste(op$save.at, "time",op$seed,".RData",sep=""))

       }# "END" OF PROCESSING MCMC SAMPLING
               
       if (iter%%1000==0 & itersave>1 ) 
       {

       #############################################################################################
       #    PROCESSING MCMC SAMPLES
       #############################################################################################
       meanmu=Mcurrent[dimX]
       meang= Mcurrent[(dimX+1):dimW]
       varg = Qcurrent[(dimX+1):dimW]/itersave

       	if(op$poly)
	{
		meanu<-Mucurrent
		pvaru<-Qucurrent/itersave
		save(iter,meanmu,meang,varg,SNPvar,meanu,pvaru,file = paste(op$save.at,"EffectsResults",op$seed,".RData",sep=""))
	}else save(iter,meanmu,meang,varg,SNPvar,file = paste(op$save.at,"EffectsResults",op$seed,".RData",sep=""))

       #  monitor MH acceptance rates over cycle for the degrees of freedom term.
       save(iter,alphadef_save,file=paste(op$save.at, "alphadef",op$seed,".RData",sep=""))

       } # "END" OF PROCESSING MCMC SAMPLING

  } # "END" OF MCMC SAMPLING

  if(length(whichNa)>0)
  {
	Z=Z0
	if(!is.null(dataobj)) X=X0
  }	
  	if(op$poly)
  	{
		BAout<-list(betahat=meanmu,ghat=meang, yhat=meanmu+Z%*%meang+Zu%*%meanu,uhat=meanu,eff_sample=effectiveSize(hyperparameters),hypers=hyperparameters)
	}else BAout<-list(betahat=meanmu,ghat=meang, yhat=meanmu+Z%*%meang,eff_sample=effectiveSize(hyperparameters),hypers=hyperparameters)
  
  class(BAout)="BAout"
  return(BAout)	
}  # END OF FUNCTION

#######################################################################################
#######################################################################################


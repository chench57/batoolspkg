\name{create.baData}
\alias{create.baData}

\title{
create.baData
}
\description{
This function combines all raw data sources and/or pre-created synbreed data object in a single, unified data object of class \code{baData}. This is a \code{list} with elements for phenotypic, genotypic, marker map, pedigree and further covariate data. All elements are optional.
}
\usage{
create.baData(synbreedobj = NULL, pheno = NULL, geno = NULL, map = NULL, 
pedigree = NULL,family = NULL, covar = NULL, reorderMap = TRUE, map.unit = "cM", 
repeated = NULL,modCovar = NULL, A = NULL, Ainv = NULL, fixed = NULL, random = NULL, 
makeA = FALSE,makeAinv = FALSE)
}

\arguments{
  \item{synbreedobj}{
	A pre-created synbreed data object
}
  \item{pheno}{
 \code{data.frame} with individuals organized in rows and traits organized in columns. For
unrepeated measures unique \code{rownames} should identify individuals. For repeated measures, the first
column identifies individuals and a second column indicates repetitions (see also argument \code{repeated}).}
  \item{geno}{
\code{matrix} with individuals organized in rows and markers organized in columns. Genotypes could be coded arbitrarily. Missing values should be coded as \code{NA}. Colums or rows with only missing values not allowed. Unique \code{rownames} identify individuals and unique \code{colnames} markers. If no \code{rownames} are available, they are taken from element \code{pheno} (if available and if dimension matches).  If no \code{colnames} are used, the \code{rownames} of \code{map} are used if dimension matches.
}
  \item{map}{
\code{data.frame} with one row for each marker and two columns (named \code{chr} and \code{pos}). First columns gives the chromosome (\code{numeric} or \code{character} but not \code{factor}) and second column the position  on the chromosome in centimorgan or the physical distance relative to the reference sequence in basepairs. Unique \code{rownames} indicate the marker names which should match with marker names in \code{geno}. Note that order and number of markers must not be identical with the order in \code{geno}. If this is the case, gaps in the map are filled with \code{NA} to ensure the same number and order as in element \code{geno} of the resulting \code{gpData} object.
}
  \item{pedigree}{
Object of class \code{pedigree}.
}
  \item{family}{
\code{data.frame} assigning individuals to families with names of individuals in \code{rownames} This information could be used for replacing of missing values with function \code{codeGeno} in synbreed.
}
  \item{covar}{
\code{data.frame} with further covariates for all individuals that either appear in \code{pheno}, \code{geno} or \code{pedigree$ID},  e.g. sex or age. \code{rownames} must be specified to identify individuals. Typically this element is not specified by the user.
}
  \item{reorderMap}{
\code{logical}. Should markers in \code{geno} and \code{map} be reordered by chromosome number and position within chromosome according to \code{map} (default = \code{TRUE})?
}
  \item{map.unit}{
Character. Unit of position in \code{map}, i.e. 'cM' for genetic distance or 'bp' for physical distance (default = 'cM').
}
  \item{repeated}{
  This column is used to identify the replications of the phenotypic values. The unique values become the names of the third dimension of the pheno object in the \code{gpData}. This argument is only required for repeated measurements.
}
  \item{modCovar}{
  \code{vector} with \code{colnames} which identify columns with covariables in \code{pheno}. This argument is only required for repeated measurements.
}
  \item{A}{
	\code{matrix} of additive genetic relationship matrix 
}
  \item{Ainv}{
	\code{matrix} of inverse of A
}
  \item{fixed}{
	\code{matrix} or \code{factor} of fixed effects
}
  \item{random}{
	\code{matrix} or \code{factor} of random effects
}
  \item{makeA}{
    \code{logical} indicating if create A matrix based on the pedigree information
}
  \item{makeAinv}{
   \code{logical} indicating if create A inverse matrix based on the pedigree information, A will be created before creating Ainv is created
}
}
\details{
 The object \code{baData} is designed to provide a unified data object for whole genome prediction. It inherits the \code{gpData} object from synbreed package and is extended with more optional data item for whole genome prediction. So \code{baData} can be used in any functions in synbreed package including data imputation and etc. 
}


\references{

}
\author{

}

\seealso{

}

\examples{
#####create baData from gpData######

data(pig) #load Michigan State University pig data
ids<-pig$covar$id[which(pig$covar$phenotyped==T & pig$covar$genotyped==T)] #find the ids
of pigs that have both genotype and phenotype
n<-length(ids) # the number of pigs in the dataset
sex<-pig$pedigree[which(pig$pedigree$ID %in% ids),]$sex # the sex effect for pigs that have phenotypes
fixed<-matrix(c(rep(1,n),sex),nrow=n,ncol=2) #create fixed effect
rownames(fixed)<-ids
f <- sample(0:5, n, replace = TRUE) #simulate random effects
f <- factor(f)	#convert to factor
ba.pig.syn<-create.baData(synbreedobj=pig,fixed=fixed,random=f,makeAinv=T) #create baData object
names(ba.pig.syn)


#####create baData from raw######
data(pig) #load Michigan State University pig data
pheno<-data.frame(pig$pheno) 
geno<-pig$geno
ped<-pig$pedigree
map=pig$map
ba.pig.raw=create.baData(pheno=pheno,geno=geno,map=map,pedigree=ped,makeAinv=F)
names(ba.pig.raw)

}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ baData }


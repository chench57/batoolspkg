#include <R.h>
#include <Rmath.h>
#include <Rdefines.h>
#include <Rinternals.h>
#include <math.h>
#include <Rconfig.h>
#include <R_ext/Lapack.h>
#include <stdio.h>
#define N 2

SEXP anteBayesAC(SEXP cassoct, SEXP cnSNP, SEXP cdimX, SEXP cnanim, SEXP cW, SEXP cWWdiag, SEXP ctheta, SEXP cycorr, SEXP cvarq, SEXP cvarE, SEXP cSNPeff, SEXP cSNPg,SEXP cmut, SEXP cvart)
{
	double *xj, *W, *WWdiag, *theta, *ycorr, *varq, *SNPeff,*assoct,*SNPg;
	double rhs=0,varE,invlhs=0,mean=0,lhs=0,sd,mut,vart;
	int SNP,i, nSNP, nanim,dimX;
    SEXP list;
	
  	GetRNGstate();
	
	nSNP=INTEGER_VALUE(cnSNP);
	nanim=INTEGER_VALUE(cnanim);
	dimX=INTEGER_VALUE(cdimX);

	varE=NUMERIC_VALUE(cvarE); 
	mut=NUMERIC_VALUE(cmut);
	vart=NUMERIC_VALUE(cvart);

	PROTECT(cassoct=AS_NUMERIC(cassoct));
	assoct=NUMERIC_POINTER(cassoct); 
		
  	PROTECT(cW=AS_NUMERIC(cW));
	W=NUMERIC_POINTER(cW); 
        
    PROTECT(cWWdiag=AS_NUMERIC(cWWdiag));
    WWdiag=NUMERIC_POINTER(cWWdiag); 

    PROTECT(ctheta=duplicate(AS_NUMERIC(ctheta)));
    theta=NUMERIC_POINTER(ctheta);  
	
    PROTECT(cycorr=duplicate(AS_NUMERIC(cycorr)));
    ycorr=NUMERIC_POINTER(cycorr);

    PROTECT(cvarq=AS_NUMERIC(cvarq));
    varq=NUMERIC_POINTER(cvarq);
	
	PROTECT(cSNPeff=AS_NUMERIC(cSNPeff));
	SNPeff=NUMERIC_POINTER(cSNPeff);


	PROTECT(cSNPg=AS_NUMERIC(cSNPg));
	SNPg=NUMERIC_POINTER(cSNPg);

	

    xj=(double *) R_alloc(nanim,sizeof(double));

		
	for(SNP=0; SNP<nSNP; SNP++)
	{		
		rhs=0;	
		for(i=0; i<nanim; i++)
		{	
			xj[i]=W[i+(SNP+dimX)*nanim];
			ycorr[i]=ycorr[i]+SNPg[SNP]*xj[i];	
			rhs=rhs+xj[i]*ycorr[i]/varE;
		}
		
		if(SNP==(nSNP-1))
		{
			lhs=WWdiag[SNP+dimX]/varE+1.0/varq[SNP];
		}
		if(SNP<(nSNP-1))
		{
			lhs=WWdiag[SNP+dimX]/varE+1.0/varq[SNP]+R_pow_di(assoct[SNP],2)/varq[SNP+1];
		}

		if(SNP==0)
		{
			rhs=rhs+assoct[SNP]*SNPg[SNP+1]/varq[SNP+1];
		}
		if(SNP==(nSNP-1))
		{
			rhs=rhs+assoct[SNP-1]*SNPg[SNP-1]/varq[SNP];
		}
		if(SNP>0 && SNP<(nSNP-1))
		{
			rhs=rhs+assoct[SNP]*SNPg[SNP+1]/varq[SNP+1]+assoct[SNP-1]*SNPg[SNP-1]/varq[SNP];
		}
		
		invlhs=1.0/lhs;
		mean=invlhs*rhs;
		sd=sqrt(invlhs);
		SNPg[SNP]= rnorm(mean,sd);		
		for(i=0; i<nanim; i++)
		{
			ycorr[i]=ycorr[i]-SNPg[SNP]*xj[i];
		}
		theta[SNP+dimX]=SNPg[SNP];
	}  
	//Rprintf("rhs %f \n",rhs);
	//Rprintf("invlhs %f \n",invlhs);

	for(SNP=0; SNP<nSNP; SNP++)
	{
		if (SNP==0) { SNPeff[SNP]=SNPg[SNP];}
        if (SNP>0) { SNPeff[SNP]=SNPg[SNP]-assoct[SNP-1]*SNPg[SNP-1];}
    }

    for(SNP=0; SNP<(nSNP-1); SNP++)
	{
        rhs=SNPg[SNP]*SNPg[SNP+1]/varq[SNP+1]+mut/vart;  
        lhs=R_pow_di(SNPg[SNP],2)/varq[SNP+1]+1/vart;
        invlhs=1/lhs;
        mean=invlhs*rhs;
		sd=sqrt(invlhs);
        assoct[SNP]=rnorm(mean,sd);
    }
		
    // Creating a list with 2 vector elements:
	PROTECT(list = allocVector(VECSXP, 7));
	// attaching theta vector to list:
	SET_VECTOR_ELT(list, 0, ctheta);
	// attaching ycorr vector to list:
	SET_VECTOR_ELT(list, 1, cycorr);
	
	SET_VECTOR_ELT(list, 2, cvarq);
	
	SET_VECTOR_ELT(list, 3, cvarE);
	// attaching SNPeff vector to list:
	SET_VECTOR_ELT(list, 4, cSNPeff);

	SET_VECTOR_ELT(list, 5, cSNPg);

	SET_VECTOR_ELT(list, 6, cassoct);


  	PutRNGstate();

  	UNPROTECT(9);
  	return(list);
	
}



SEXP anteBayesBC(SEXP cassoct, SEXP cnSNP, SEXP cdimX, SEXP cnanim, SEXP cW, SEXP cM, SEXP cWWdiag, SEXP ctheta, SEXP cycorr, SEXP cvarq, SEXP cvarE, SEXP cSNPeff, SEXP cSNPg, SEXP cpostprob,SEXP cscalea,SEXP cdef, SEXP calphametrovar, SEXP cpi,SEXP cG1, SEXP cmut, SEXP cvart)
{
	double *xj, *W,*M, *theta, *ycorr, *varq, *SNPeff,*assoct,*SNPg,*alphametrovar;
	double rhs=0,varE,invlhs=0,mean=0,lhs=0,sd,mut,vart,def,scalea,pi,MpM;
	double u,v_no, v_yes,logDataNullModel,logDataOld,alphacheck=0,varCandidate=0,logDataNew,acceptProb;
	int SNP,i, nSNP ,*postprob, nanim,*G1,dimX, mhiter;
    SEXP list,alphacheck1;
	
  	GetRNGstate();
	
	nSNP=INTEGER_VALUE(cnSNP);
	nanim=INTEGER_VALUE(cnanim);
	dimX=INTEGER_VALUE(cdimX);

	varE=NUMERIC_VALUE(cvarE); 
	mut=NUMERIC_VALUE(cmut);
	vart=NUMERIC_VALUE(cvart);
	
	def=NUMERIC_VALUE(cdef);
	scalea=NUMERIC_VALUE(cscalea);
	
	pi=NUMERIC_VALUE(cpi);
	
	PROTECT(cG1=AS_INTEGER(cG1));
	G1=INTEGER_POINTER(cG1);

	PROTECT(cassoct=AS_NUMERIC(cassoct));
	assoct=NUMERIC_POINTER(cassoct); 
		
  	PROTECT(cW=AS_NUMERIC(cW));
	W=NUMERIC_POINTER(cW); 
	
	PROTECT(cM=AS_NUMERIC(cM));
	M=NUMERIC_POINTER(cM);
        
    //PROTECT(cWWdiag=AS_NUMERIC(cWWdiag));
    //WWdiag=NUMERIC_POINTER(cWWdiag); 

    PROTECT(ctheta=duplicate(AS_NUMERIC(ctheta)));
    theta=NUMERIC_POINTER(ctheta);  
	
    PROTECT(cycorr=duplicate(AS_NUMERIC(cycorr)));
    ycorr=NUMERIC_POINTER(cycorr);

    PROTECT(cvarq=AS_NUMERIC(cvarq));
    varq=NUMERIC_POINTER(cvarq);
	
	PROTECT(cSNPeff=AS_NUMERIC(cSNPeff));
	SNPeff=NUMERIC_POINTER(cSNPeff);


	PROTECT(cSNPg=AS_NUMERIC(cSNPg));
	SNPg=NUMERIC_POINTER(cSNPg);
	
	PROTECT(cpostprob=AS_INTEGER(cpostprob));
	postprob=INTEGER_POINTER(cpostprob);

	PROTECT(calphametrovar=AS_NUMERIC(calphametrovar));
	alphametrovar=NUMERIC_POINTER(calphametrovar);
	
	PROTECT(alphacheck1 = allocVector(REALSXP,1));
	

    xj=(double *) R_alloc(nanim,sizeof(double));

		
	for(SNP=(nSNP-1); SNP>=0; SNP--)
	{		
	
		for(i=0; i<nanim; i++)
		{	
			xj[i]=M[i+SNP*nanim];
			ycorr[i]+=SNPeff[SNP]*xj[i];				
		}		
		if(SNP==(nSNP-1))
		{
			for(i=0; i<nanim; i++)
			{
				M[i+SNP*nanim]=W[i+(SNP+dimX)*nanim];
			}
		}
	
		if(SNP<(nSNP-1))
		{
			for(i=0; i<nanim; i++)
			{
				M[i+SNP*nanim]=M[i+(SNP+1)*nanim]*assoct[SNP]+W[i+(SNP+dimX)*nanim];
			}
		}
		rhs=0;
			
		for(i=0; i<nanim; i++)
		{	
			rhs+=M[i+SNP*nanim]*ycorr[i];			
		}		
		
		MpM=0;
		for(i=0; i<nanim; i++)
		{	
			MpM+=M[i+SNP*nanim]*M[i+SNP*nanim];			
		}		

		v_no=MpM*varE;
		v_yes=MpM*MpM*varq[SNP]+v_no;
		logDataNullModel=-0.5*(log(v_no)+R_pow_di(rhs,2)/v_no);
		if(varq[SNP]>0.0)
		{ 
			logDataOld   = -0.5*(log(v_yes) + R_pow_di(rhs,2)/v_yes); 
		}
		else
		{
			logDataOld = logDataNullModel;
		}
		alphacheck=0;
		for (mhiter=0;mhiter<10;mhiter++)
		{
			u = runif(0.0,1.0);
			varCandidate=0;
			if(u<pi)
			{
				varCandidate=scalea*def/rchisq(def);
			}
			if(varCandidate>0.0)
			{
				v_yes=MpM*MpM*varCandidate+v_no;
				logDataNew= -0.5*(log(v_yes) + rhs*rhs/v_yes);
			}
			else
			{
				logDataNew=logDataNullModel;
			}
			
			acceptProb=exp(logDataNew-logDataOld);
			u = runif(0.0,1.0);
			if(u<acceptProb)
			{
				varq[SNP]=varCandidate;
				logDataOld=logDataNew;
			}
			alphacheck+=acceptProb;		
		}
		alphametrovar[SNP]+=alphacheck/10;
		
		if(varq[SNP]>0)
		{
			G1[0]++;
			postprob[SNP]++;
			lhs=MpM/varE+1.0/varq[SNP];
			invlhs=1/lhs;
			mean=invlhs*rhs/varE;
			sd=sqrt(invlhs);
			SNPeff[SNP]=rnorm(mean,sd);
			for(i=0; i<nanim; i++)
			{	
				ycorr[i]-=	M[i+SNP*nanim]*SNPeff[SNP];	
			}			
		}
		else if(varq[SNP]==0)
		{
			SNPeff[SNP]=0.0;
		}		
	}	
	
	for (SNP=0;SNP<nSNP;SNP++)
	{
		if (SNP==0) { SNPg[SNP]=SNPeff[SNP];}
		if (SNP > 0) { SNPg[SNP]=assoct[SNP-1]*SNPg[SNP-1]+SNPeff[SNP];}
		theta[dimX+SNP] = SNPg[SNP];
    }
	
    for(SNP=0; SNP<(nSNP-1); SNP++)
	{
        if(varq[SNP+1]!=0)
		{
			rhs=SNPg[SNP]*SNPg[SNP+1]/varq[SNP+1]+mut/vart;  
			lhs=(SNPg[SNP]*SNPg[SNP])/varq[SNP+1]+1/vart;
			invlhs=1/lhs;
			mean=invlhs*rhs;
			sd=sqrt(invlhs);
			assoct[SNP]=rnorm(mean,sd);
		}
		if (varq[SNP+1]==0 && SNPg[SNP]!=0)
		{
            assoct[SNP]=SNPg[SNP+1]/SNPg[SNP];
		}
		if (varq[SNP+1]==0 && SNPg[SNP]==0)
		{
			assoct[SNP]=rnorm(mut,sqrt(vart));
		}		
    }
		
    // Creating a list with 2 vector elements:
	PROTECT(list = allocVector(VECSXP, 14));
	
	SET_VECTOR_ELT(list, 0, ctheta);
	// attaching ycorr vector to list:
	SET_VECTOR_ELT(list, 1, cycorr);
	
	SET_VECTOR_ELT(list, 2, cvarq);
	
	SET_VECTOR_ELT(list, 3, cvarE);
	// attaching SNPeff vector to list:
	SET_VECTOR_ELT(list, 4, cSNPeff);
	
	SET_VECTOR_ELT(list, 5, cpostprob);
	
	SET_VECTOR_ELT(list, 6, calphametrovar);

	SET_VECTOR_ELT(list, 7, cG1);
	
	SET_VECTOR_ELT(list, 8, cdef);
	
	SET_VECTOR_ELT(list, 9, cscalea);
	
	SET_VECTOR_ELT(list, 10, cpi);
	
	REAL(alphacheck1)[0]=alphacheck;
	
	
	SET_VECTOR_ELT(list, 11, alphacheck1);	

	SET_VECTOR_ELT(list, 12, cSNPg);

	SET_VECTOR_ELT(list, 13, cassoct);


  	PutRNGstate();

  	UNPROTECT(13);
  	return(list);
	
}


SEXP BayesAC(SEXP cnSNP, SEXP cdimX, SEXP cnanim, SEXP cW, SEXP cWWdiag, SEXP ctheta, SEXP cycorr, SEXP cvarq, SEXP cvarE, SEXP cSNPeff)
{
	double *xj, *W, *WWdiag, *theta, *ycorr, *varq, *SNPeff;
	double rhs,varE,lhs;
	int j,i, nSNP, nanim,dimX;
    SEXP list;
	
  	GetRNGstate();
	
	nSNP=INTEGER_VALUE(cnSNP);
	nanim=INTEGER_VALUE(cnanim);
	dimX=INTEGER_VALUE(cdimX);

	varE=NUMERIC_VALUE(cvarE); 
		
  	PROTECT(cW=AS_NUMERIC(cW));
	W=NUMERIC_POINTER(cW); 
        
    PROTECT(cWWdiag=AS_NUMERIC(cWWdiag));
    WWdiag=NUMERIC_POINTER(cWWdiag); 

    PROTECT(ctheta=duplicate(AS_NUMERIC(ctheta)));
    theta=NUMERIC_POINTER(ctheta);  
	
    PROTECT(cycorr=duplicate(AS_NUMERIC(cycorr)));
    ycorr=NUMERIC_POINTER(cycorr);

    PROTECT(cvarq=AS_NUMERIC(cvarq));
    varq=NUMERIC_POINTER(cvarq);
	
	PROTECT(cSNPeff=AS_NUMERIC(cSNPeff));
	SNPeff=NUMERIC_POINTER(cSNPeff);

    xj=(double *) R_alloc(nanim,sizeof(double));

			
	for(j=0; j<nSNP; j++)
	{		
		rhs=0;
		for(i=0; i<nanim; i++)
		{	
			xj[i]=W[i+(j+dimX)*nanim];
			ycorr[i]=ycorr[i]+theta[j+dimX]*xj[i];
			rhs+=xj[i]*ycorr[i];
		}
		
		rhs=rhs/varE;
		lhs=WWdiag[j+dimX]/varE+1.0/varq[j];
		SNPeff[j]=rhs/lhs + sqrt(1.0/lhs)*rnorm(0,1) ;
		theta[j+dimX]=SNPeff[j];

		for(i=0; i<nanim; i++)
		{
			ycorr[i]=ycorr[i]-theta[j+dimX]*xj[i];
		}
		
	}  
		
    // Creating a list with 2 vector elements:
	PROTECT(list = allocVector(VECSXP, 5));
	// attaching theta vector to list:
	SET_VECTOR_ELT(list, 0, ctheta);
	// attaching ycorr vector to list:
	SET_VECTOR_ELT(list, 1, cycorr);
	
	SET_VECTOR_ELT(list, 2, cvarq);
	
	SET_VECTOR_ELT(list, 3, cvarE);
	// attaching SNPeff vector to list:
	SET_VECTOR_ELT(list, 4, cSNPeff);
  	PutRNGstate();

  	UNPROTECT(7);
  	return(list);
	
}


SEXP BayesBC(SEXP cnSNP, SEXP cdimX, SEXP cnanim, SEXP cW, SEXP cWWdiag, SEXP ctheta, SEXP cycorr, SEXP cvarq, SEXP cvarE, SEXP cSNPeff, SEXP cpostprob,SEXP cscalea,SEXP cdef, SEXP calphametrovar,SEXP citer, SEXP cpi, SEXP cG1)
{
	int  *postprob,inc=1;
	double *W, *WWdiag,*xj,*theta, *ycorr, *varq, *SNPeff,*alphametrovar;
	double rhs,varE,invlhs,mean,lhs,sd,v_no,v_yes,logDataNullModel;
	double logDataOld,alphacheck=0,def,scalea,acceptProb=0;
	double logDataNew,varCandidate,u,pi,b;
	int SNP,nSNP,nanim,*G1,dimX,mhiter;
    	SEXP list,alphacheck1,alpha2;
	
  	GetRNGstate();
	
	nSNP=INTEGER_VALUE(cnSNP);
	nanim=INTEGER_VALUE(cnanim);
	dimX=INTEGER_VALUE(cdimX);
	def=NUMERIC_VALUE(cdef);
	//iter=INTEGER_VALUE(citer);
	//G1=INTEGER_VALUE(cG1);
	
	scalea=NUMERIC_VALUE(cscalea);
	varE=NUMERIC_VALUE(cvarE); 
	pi=NUMERIC_VALUE(cpi);
	

		//G1=INTEGER_VALUE(cG1);
		
	PROTECT(cG1=AS_INTEGER(cG1));
	G1=INTEGER_POINTER(cG1); 	
	
  	PROTECT(cW=AS_NUMERIC(cW));
	W=NUMERIC_POINTER(cW); 
        
    PROTECT(cWWdiag=AS_NUMERIC(cWWdiag));
    WWdiag=NUMERIC_POINTER(cWWdiag); 

    PROTECT(ctheta=duplicate(AS_NUMERIC(ctheta)));
    theta=NUMERIC_POINTER(ctheta);  
	
    PROTECT(cycorr=duplicate(AS_NUMERIC(cycorr)));
    ycorr=NUMERIC_POINTER(cycorr);

    PROTECT(cvarq=AS_NUMERIC(cvarq));
    varq=NUMERIC_POINTER(cvarq);
	
	PROTECT(cSNPeff=AS_NUMERIC(cSNPeff));
	SNPeff=NUMERIC_POINTER(cSNPeff);
	
	PROTECT(cpostprob=AS_INTEGER(cpostprob));
	postprob=INTEGER_POINTER(cpostprob);
	
	PROTECT(calphametrovar=AS_NUMERIC(calphametrovar));
	alphametrovar=NUMERIC_POINTER(calphametrovar);
	
	PROTECT(alpha2 = allocVector(REALSXP,1));
	
	PROTECT(alphacheck1 = allocVector(REALSXP,1));

    //xj=(int *) R_alloc(nanim,sizeof(int));

		
	for(SNP=0; SNP<nSNP; SNP++)
	{
		b=theta[SNP+dimX];
		xj=W+(SNP+dimX)*nanim;
		F77_NAME(daxpy)(&nanim,&b,xj,&inc,ycorr,&inc);
		rhs=F77_NAME(ddot)(&nanim,xj,&inc,ycorr,&inc);
		/*rhs=0;
		for(i=0; i<nanim; i++)
		{	
			xj[i]=W[i+(SNP+dimX)*nanim];
			ycorr[i]=ycorr[i]+theta[SNP+dimX]*xj[i];
			rhs=rhs+xj[i]*ycorr[i];
		}
		*/
		v_no=WWdiag[SNP+dimX]*varE;
		v_yes=R_pow_di(WWdiag[SNP+dimX],2)*varq[SNP]+v_no; //R_pow_di(x,2)=x^2

		logDataNullModel =   -0.5*(log(v_no)+R_pow_di(rhs,2)/v_no);
		
		if (varq[SNP]>0.0)
		{
			logDataOld =  -0.5*(log(v_yes)+R_pow_di(rhs,2)/v_yes);
		}
		else
		{
			logDataOld=logDataNullModel;
		}
		
		alphacheck=0;
		for (mhiter=0; mhiter<10;mhiter++) 
		{  
			u = runif(0.0,1.0);
			varCandidate=0;
			if(u<pi)
			{
				varCandidate=scalea*def/(rchisq(def));
			}
			if(varCandidate>0.0)
			{
				v_yes =R_pow_di(WWdiag[dimX+SNP],2)*varCandidate + v_no;
				logDataNew = -0.5*(log(v_yes)+R_pow_di(rhs,2)/v_yes);
			}
			else
			{
				logDataNew = logDataNullModel;					
			}

			acceptProb = exp(logDataNew-logDataOld);
			u=runif(0,1);
			if(u<acceptProb)
			{
				varq[SNP]=varCandidate;
                logDataOld=logDataNew;
			}	
			alphacheck=alphacheck+acceptProb;
				
		}
		alphametrovar[SNP] = alphametrovar[SNP] + alphacheck/10;
		if(varq[SNP]>0.0)
		{
			G1[0]=G1[0]+1;
			postprob[SNP]=postprob[SNP]+1;
	
			lhs=WWdiag[SNP+dimX]/varE+1.0/varq[SNP];
			invlhs=1.0/lhs;
			mean=invlhs*rhs/varE;
			sd=sqrt(invlhs);
			SNPeff[SNP]= rnorm(mean,sd);
			theta[SNP+dimX]=SNPeff[SNP];
			b=-theta[SNP+dimX];
			F77_NAME(daxpy)(&nanim,&b,xj,&inc,ycorr,&inc);
			/*
			for(i=0; i<nanim; i++)
			{		
				ycorr[i]=ycorr[i]-theta[SNP+dimX]*xj[i];
			} 
			*/
		}
		else if (varq[SNP]==0.0)
		{
			SNPeff[SNP]=0.0;
			theta[dimX+SNP]=0.0;
		}
	}

	
				
		
	//Rprintf("G1 in C %d",G1[0]); 
		
    // Creating a list with 2 vector elements:
	PROTECT(list = allocVector(VECSXP, 13));
	// attaching theta vector to list:
	SET_VECTOR_ELT(list, 0, ctheta);
	// attaching ycorr vector to list:
	SET_VECTOR_ELT(list, 1, cycorr);
	
	SET_VECTOR_ELT(list, 2, cvarq);
	
	SET_VECTOR_ELT(list, 3, cvarE); //do not need to return
	// attaching SNPeff vector to list:
	SET_VECTOR_ELT(list, 4, cSNPeff);
	
	SET_VECTOR_ELT(list, 5, cpostprob);
	
	SET_VECTOR_ELT(list, 6, calphametrovar);
	
	SET_VECTOR_ELT(list, 7, cG1);
	
	SET_VECTOR_ELT(list, 8, cdef);
	
	SET_VECTOR_ELT(list, 9, cscalea);
	
	SET_VECTOR_ELT(list, 10, cpi);
	
	REAL(alpha2)[0]=acceptProb;
	
	REAL(alphacheck1)[0]=alphacheck;
	
	SET_VECTOR_ELT(list, 11, alpha2);
	
	SET_VECTOR_ELT(list, 12, alphacheck1);
	
	
  	PutRNGstate();

  	UNPROTECT(12);
  	return(list);
}


SEXP BayesACL(SEXP cnSNP, SEXP cdimX, SEXP cnanim, SEXP cW, SEXP cWWdiag, SEXP ctheta, SEXP cycorr, SEXP cvarq, SEXP cvarE, SEXP cSNPeff)
{
	double *xj, *W, *WWdiag, *theta, *ycorr, *varq, *SNPeff;
	double rhs,varE,lhs,b;
	int j,i, nSNP, nanim,dimX,inc=1;
    SEXP list;
	
  	GetRNGstate();
	
	nSNP=INTEGER_VALUE(cnSNP);
	nanim=INTEGER_VALUE(cnanim);
	dimX=INTEGER_VALUE(cdimX);

	varE=NUMERIC_VALUE(cvarE); 
		
  	PROTECT(cW=AS_NUMERIC(cW));
	W=NUMERIC_POINTER(cW); 
        
    PROTECT(cWWdiag=AS_NUMERIC(cWWdiag));
    WWdiag=NUMERIC_POINTER(cWWdiag); 

    PROTECT(ctheta=duplicate(AS_NUMERIC(ctheta)));
    theta=NUMERIC_POINTER(ctheta);  
	
    PROTECT(cycorr=duplicate(AS_NUMERIC(cycorr)));
    ycorr=NUMERIC_POINTER(cycorr);

    PROTECT(cvarq=AS_NUMERIC(cvarq));
    varq=NUMERIC_POINTER(cvarq);
	
	PROTECT(cSNPeff=AS_NUMERIC(cSNPeff));
	SNPeff=NUMERIC_POINTER(cSNPeff);

    //xj=(double *) R_alloc(nanim,sizeof(double));

			
	for(j=0; j<nSNP; j++)
	{		
		b=theta[j+dimX];
		xj=W+(j+dimX)*nanim;
		F77_NAME(daxpy)(&nanim,&b,xj,&inc,ycorr,&inc);
		rhs=F77_NAME(ddot)(&nanim,xj,&inc,ycorr,&inc);
		for(i=0; i<nanim; i++)
		{	
			xj[i]=W[i+(j+dimX)*nanim];
			ycorr[i]=ycorr[i]+theta[j+dimX]*xj[i];
			rhs+=xj[i]*ycorr[i];
		}
		rhs=rhs/varE;
		lhs=WWdiag[j+dimX]/varE+1.0/varq[j];
		SNPeff[j]=rhs/lhs + sqrt(1.0/lhs)*rnorm(0,1) ;
		theta[j+dimX]=SNPeff[j];
		b=-theta[j+dimX];
		F77_NAME(daxpy)(&nanim,&b,xj,&inc,ycorr,&inc);
		//for(i=0; i<nanim; i++)
		//{
		//	ycorr[i]=ycorr[i]-theta[j+dimX]*xj[i];
		//}
		
	}  
		
    // Creating a list with 2 vector elements:
	PROTECT(list = allocVector(VECSXP, 5));
	// attaching theta vector to list:
	SET_VECTOR_ELT(list, 0, ctheta);
	// attaching ycorr vector to list:
	SET_VECTOR_ELT(list, 1, cycorr);
	
	SET_VECTOR_ELT(list, 2, cvarq);
	
	SET_VECTOR_ELT(list, 3, cvarE);
	// attaching SNPeff vector to list:
	SET_VECTOR_ELT(list, 4, cSNPeff);
  	PutRNGstate();

  	UNPROTECT(7);
  	return(list);
	
}




SEXP sample_beta(SEXP n, SEXP pL, SEXP XL, SEXP xL2, SEXP bL, SEXP e, SEXP varBj, SEXP varE, SEXP minAbsBeta)
{
        double *xj, *pXL, *pxL2, *pbL, *pe, *pvarBj;
	double rhs,c,sigma2e, smallBeta;
	
        int j,i, rows, cols;
        SEXP list;
	
  	GetRNGstate();
	
	rows=INTEGER_VALUE(n);
	cols=INTEGER_VALUE(pL);
	sigma2e=NUMERIC_VALUE(varE);
	smallBeta=NUMERIC_VALUE(minAbsBeta);
	
  	PROTECT(XL=AS_NUMERIC(XL));
        pXL=NUMERIC_POINTER(XL);

        PROTECT(xL2=AS_NUMERIC(xL2));
        pxL2=NUMERIC_POINTER(xL2);

        PROTECT(bL=AS_NUMERIC(bL));
        pbL=NUMERIC_POINTER(bL);

        PROTECT(e=AS_NUMERIC(e));
        pe=NUMERIC_POINTER(e);

        PROTECT(varBj=AS_NUMERIC(varBj));
        pvarBj=NUMERIC_POINTER(varBj);

        xj=(double *) R_alloc(rows,sizeof(double));

        for(j=0; j<cols;j++)
        {
	  rhs=0;
	  for(i=0; i<rows; i++)
	  {
	    xj[i]=pXL[i+j*rows];
	    pe[i] = pe[i] + pbL[j]*xj[i];
	    rhs+=xj[i]*pe[i];
	  }
	  rhs=rhs/sigma2e;
  	  c=pxL2[j]/sigma2e + 1.0/pvarBj[j];
	  pbL[j]=rhs/c + sqrt(1.0/c)*norm_rand();
	  
	  for(i=0; i<rows; i++)
	  {
	    pe[i] = pe[i] - pbL[j]*xj[i];
	  }
          if(fabs(pbL[j])<smallBeta)
          {
             pbL[j]=smallBeta;
          }
        }
        
        // Creating a list with 2 vector elements:
	PROTECT(list = allocVector(VECSXP, 2));
	// attaching bL vector to list:
	SET_VECTOR_ELT(list, 0, bL);
	// attaching e vector to list:
	SET_VECTOR_ELT(list, 1, e);

  	PutRNGstate();

  	UNPROTECT(6);

  	return(list);
}


SEXP BayesCC(SEXP cnSNP, SEXP cdimX, SEXP cnanim, SEXP cW, SEXP cWWdiag, SEXP ctheta, SEXP cycorr, SEXP cvarq, SEXP cvarE, SEXP cSNPeff,SEXP cpi,SEXP cphi,SEXP chratio)
{
	double *xj, *W, *WWdiag, *theta, *ycorr, *varq, *SNPeff,*phi,*hratio;
	double rhs,varE,lhs,pi,lambda;
	int j,i, nSNP, nanim,dimX;
	double h1,h2,logh,varG; //for phi
    SEXP list;
	
  	GetRNGstate();
	
	nSNP=INTEGER_VALUE(cnSNP);
	nanim=INTEGER_VALUE(cnanim);
	dimX=INTEGER_VALUE(cdimX);

	varE=NUMERIC_VALUE(cvarE); 
	pi=NUMERIC_VALUE(cpi);

		
  	PROTECT(cW=AS_NUMERIC(cW));
	W=NUMERIC_POINTER(cW); 
        
    PROTECT(cWWdiag=AS_NUMERIC(cWWdiag));
    WWdiag=NUMERIC_POINTER(cWWdiag); 

    PROTECT(ctheta=duplicate(AS_NUMERIC(ctheta)));
    theta=NUMERIC_POINTER(ctheta);  
	
    PROTECT(cycorr=duplicate(AS_NUMERIC(cycorr)));
    ycorr=NUMERIC_POINTER(cycorr);

    PROTECT(cvarq=AS_NUMERIC(cvarq));
    varq=NUMERIC_POINTER(cvarq);
	
	PROTECT(cSNPeff=AS_NUMERIC(cSNPeff));
	SNPeff=NUMERIC_POINTER(cSNPeff);
	
	

	
	PROTECT(cphi=AS_NUMERIC(cphi));
	phi=NUMERIC_POINTER(cphi);
	

	
	PROTECT(chratio=AS_NUMERIC(chratio));
	hratio=NUMERIC_POINTER(chratio);


        xj=(double *) R_alloc(nanim,sizeof(double));


	varG=varq[0];		
	for(j=0; j<nSNP; j++)
	{		
		rhs=0;
		h1=log1p(WWdiag[j+dimX]*varG/varE)-log1p(WWdiag[j+dimX]*varG/varE/100);
		h2=1/(WWdiag[j+dimX]/varE+100/varG)-1/(WWdiag[j+dimX]/varE+1/varG);
		//Rprintf("h1=%f\n",h1);
		//Rprintf("h2=%f\n",h2);
		for(i=0; i<nanim; i++)
		{	
			xj[i]=W[i+(j+dimX)*nanim];
			ycorr[i]=ycorr[i]+theta[j+dimX]*xj[i];
			rhs+=xj[i]*ycorr[i];
			//Rprintf("rhs=%f\n",rhs);
		}
		logh=0.5*(h1+h2*R_pow_di(rhs/varE,2));
		//Rprintf("rhs=%f\n",rhs);
		//Rprintf("vare=%f\n",varE);
		
		//Rprintf("h2=%f\n",h2);
		hratio[j]=R_pow(M_E,0.5*logh);
		//Rprintf("hratio=%f\n",hratio);
		phi[j]=rbinom(1,pi/(hratio[j]*(1-pi)+pi));
		//phi[j]=rbinom(1,pi/(0.101005*(1-pi)+pi));
		//Rprintf("phi=%f\n",phi);
		
		rhs=rhs/varE;
		lhs=WWdiag[j+dimX]/varE+1.0/(varG*(0.01+0.99*phi[j]));
		SNPeff[j]=rhs/lhs + sqrt(1.0/lhs)*rnorm(0,1) ;
		theta[j+dimX]=SNPeff[j];

		for(i=0; i<nanim; i++)
		{
			ycorr[i]=ycorr[i]-theta[j+dimX]*xj[i];
		}
		
	}  
		
    // Creating a list with 2 vector elements:
	PROTECT(list = allocVector(VECSXP, 7));
	// attaching theta vector to list:
	SET_VECTOR_ELT(list, 0, ctheta);
	// attaching ycorr vector to list:
	SET_VECTOR_ELT(list, 1, cycorr);
	
	SET_VECTOR_ELT(list, 2, cvarq);
	
	SET_VECTOR_ELT(list, 3, cvarE);
	// attaching SNPeff vector to list:
	SET_VECTOR_ELT(list, 4, cSNPeff);
	
	SET_VECTOR_ELT(list, 5, cphi);
	
	SET_VECTOR_ELT(list, 6, chratio);
  	PutRNGstate();

  	UNPROTECT(9);
  	return(list);
	
}


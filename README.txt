Installation instructions:
To install the package, first make sure you installed the following packages in R:
install.packages(“coda”)
install.packages(“msm”)
install.packages(“synbreed”)

Then use the following command in command line under the git folder to install BATools:
R CMD INSTALL BATools
(RTools http://cran.r-project.org/bin/windows/Rtools/ on Windows in required for this)

To use the manual, you can use the pdf file or type the following:
library(BATools)
help(BATools)
help(create.baData)
help(create.options)
help(basummary)
help(baplot)